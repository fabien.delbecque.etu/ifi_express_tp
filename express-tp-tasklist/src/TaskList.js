/**
 * Sort a task by their start time and their due time
 * 
 * @param {Task} taskA is the first one
 * @param {Task} taskB is the second one
 * 
 * @returns {Number} taskA is before {-1} or after {1} taskB
 */
function defaultCriterium( taskA, taskB ){
    
    if( taskA.start && taskB.start ){
        return taskA.start - taskB.start;
    } else if( taskA.start ){
        return -1;
    } else if( taskB.start ){
        return 1;
    }

    if( taskA.duetime && taskB.duetime ){
        return taskA.duetime - taskB.duetime;
    } else if( taskA.duetime ){
        return -1;
    } else if( taskB.duetime ){
        return 1;
    }

    return 0;
}

module.exports.TaskList = class {

    /**
     * Create a task list
     * 
     * @param {String} name is the task list name
     * @param {Number[]} list is the default task for the task list 
     */
    constructor( name, list = [] ){
        this.name = name;
        this.list = list;
    }

    /**
     * @returns true if the task has zero task
     */
    isEmpty(){
        return this.list.length === 0;
    }

    /**
     * Add a task to the list
     * 
     * @param {Number} taskId
     */
    add( taskId ){
        this.list.push( taskId );
    }

    /**
     * Delete some task from the task list
     * 
     * @param {Function} mustBeDeleted is the function who determine which task we should keep
     */
    removeIf( mustBeDeleted ) {
        let keepIt = [];

        this.list.forEach((element) => {
            if( ! mustBeDeleted( element ) ){
                keepIt.push( element );
            }
        });

        this.list = keepIt;
    }

    /**
     * Remove a task from the TaskList
     * 
     * @param {Number} taskId is the task toitem.toString()be deleted 
     */
    remove( taskId ){
        this.removeIf( (anotherTask) => taskId == anotherTask );
    }

    /**
     * Sort the task list
     * 
     * @param {Function} criterium is the criterium to determine the order of the list
     * @param {Boolean} duplicate determine if we should create a new task list or not
     * 
     * @returns {TaskList} only when duplicate is true, create a new task list from the sorted list
     */
    sort( criterium = defaultCriterium, duplicate = false ){
    
        let sortedTask = this.task.sort( criterium );

        if( duplicate ){
            return new TaskList( sortedTask );
        } else {
            this.list = sortedTask;
        }

        return null;
    }

    /**
     * @returns {String} print the task list
     */
    toString(){

        let print = `Liste des taches "${ this.name }"`;

        return print;
    }

    /**
     * Contains this task id
     * 
     * @param {Number} id is the task id
     */
    has( id ){

        for (const _id of this.list) {
            if( id === _id ){
                return true;
            }
        }

        return false;
    }
};